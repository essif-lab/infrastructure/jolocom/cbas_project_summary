CBAS project summary
====================

## **IOC-1 - Project Summary of CBAS - Capability BasedAuthorization System**
For more information, please contact:


- Irene Adamski (irene@jolocom.com)
- Kai Wagner (kai@jolocom.com)
- Eugeniu Rusu (eugeniu@jolocom.com)



## **Introduction**

### **About Jolocom**

Jolocom was founded in 2014 for the express purpose to support and accelerate the adoption & development of Self-Sovereign digital Identities. Since then we have established ourselves as a reliable partner in both the private and public sector via various successful R&D pilot projects and the first tailor-made SSI solutions for corporations and organisations. Within the wider SSI community we are involved with various associations (W3C, DIF, INATBA, VSDI, Bundesblock) to advance interoperability and standardisation equally. Our Articles of Association reflect this for-purpose mindset and lend credibility to our support of interoperability by precluding M&As. (96)

### **Capability BasedAuthorization System (CBAS)**
The proposed SSI component is an implementation of a capabilities based authorization system, utilizing DIDs, Verifiable Credentials, Verifiable Presentations, DID Comm (or JSON Web Messages), etc. We believe that numerous relevant use cases (e.g. delegation, complex authorization logic, automated operations, etc.) can be simplified given a simple and extendible way to define and communicate credentials encoding object capabilities. The intention is to focus on developing a usable implementation (alongside an open source reference integration with the Jolocom SmartWallet and Jolocom Library) and avoid defining new standards wherever possible. As part of evaluating prior art in the field, we have reviewed the [ZCAP-LD CCG Draft](https://github.com/w3c-ccg/zcap-ld), [RWOTC5 Linked Data Capabilities](https://github.com/WebOfTrustInfo/rwot5-boston/blob/master/final-documents/lds-ocap.md), and [Aries RFCS 0103](https://github.com/hyperledger/aries-rfcs/tree/33dc2a8b3235a54ecb483f6020bb20b977918978/concepts/0103-indirect-identity-control) and [0104](https://github.com/hyperledger/aries-rfcs/tree/33dc2a8b3235a54ecb483f6020bb20b977918978/concepts/0104-chained-credentials), which, although not fully finalized, have served as valuable input documents. We intend to align the development efforts with emerging standardization efforts, most notably the [Authentic Chained Data Container](https://wiki.trustoverip.org/display/HOME/ACDC+%28Authentic+Chained+Data+Container%29+Task+Force) task force active in the Trust Over Ip Foundation.


## **Summary**

### **Business Problem**

We intend for the proposed solution to be a building block, used to define higher level, use case specific SSI based authorization systems. Access control and authorization are at the foundation of numerous SSI use cases which go beyond simple credential / document exchange. We hope to make these use cases more attainable given the proposed component. Examples of such use cases might include, but are not limited to, indirect identity control flows (i.e. delegation/guardianship/controllership as defined in Aries RFC 0103), and more generally managing permissions between entities (e.g. Wallets, IoT Devices, DIF Identity Hubs, Aries Agents), either across identity boundaries and internally within one's identity domain. Most SSI companies and services will need a similar component and could therefore greatly benefit from the proposed solution.  

### **Technical Solution**

Verifiable Credentials (VC's) are a well understood and adopted concept in SSI. By encoding object capabilities using VC's, we hope to reuse existing tooling (e.g. libraries for creating/validating VC's), infrastructure (e.g. the Universal Issuer, Universal Verifier), and components (e.g. Wallets to hold, share, and issue capabilities). Furthermore, existing protocols for Credential issuance/share/revocation (ideally based on DID-Comm/JWM) can be used to communicate capabilities securely between parties. We would like to avoid defining new standards but instead find a convergent stack of existing specifications. We hope to inherit extensibility from the underlying specifications.  We intend to develop an open source library for creating and evaluating credentials encoding object capabilities, and make it available with permissive licensing. We also intend to integrate the aforementioned library with our open source SSI library, as well as our open source Wallet application. 



### **Integration with the eSSIF-Lab Functional Architecture and Community**

Our intention is for the CBAS module is to be easily integratable with various SSI deployments (e.g. DID Method agnostic, different VC exchange protocols). This can be partially achieved by following open / well adopted specifications and utilizing existing concepts and infrastructure (e.g. Universal Issuer / Verifier / Resolver).
We hope that this approach will also allow us to reuse various existing components and libraries exposed through the “[SSI Protocols and Crypto layer](https://essif-lab.pages.grnet.gr/framework/docs/functional-architecture#23--ssi-protocols-and-crypto-layer)”, and achieve much simpler integration with existing WHIV components (e.g. with regards to UI and UX flows).

Beneficial collaborations / synergies with other ESSIF lab participants:
- PolicyMan intends to allow resource owners to specify in controlled natural language their policies for granting access to users who possess verifiable credentials. This maps quite closely to the goals of the CBAS library, perhaps policies related to access to restricted resources can be expressed using the controlled natural language developed by this partner.
- TRAIN provides a method for what some call "Extended Verification" of credentials. It might be beneficial to explore how various application specific authorization policies can be modelled as Trust Schemes and Trust Policies, as defined by the TRAIN proposal.